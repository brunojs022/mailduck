<?php

namespace App\Middlewares;

class AdminMidleware {

    public function __invoke($request, $response, $next) {
        if(!isset($_SESSION['logged'])) {
            return $response->withRedirect(URL . '/admin/login');
        }
        $response = $next($request, $response);
        return $response;
    }

}