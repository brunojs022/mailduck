<?php

namespace App\Controllers;

final class LoginController extends DefaultController {

    public function index($request, $response, array $args) {
        if (isset($_SESSION['logged'])) {
            return $response->withRedirect(URL . '/admin');
        }
        return $this->view->render($response, 'admin/login.phtml');
    }

    public function login($request, $response, array $args) {
        $data = $request->getParsedBody();
        $email = strip_tags(filter_var($data['email'], FILTER_SANITIZE_STRING));
        $pass = md5(strip_tags(filter_var($data['pass'], FILTER_SANITIZE_STRING)));

        $sql = 'select * from users where email = ? and password = ?';
        $sth = $this->db->prepare($sql);
        $sth->execute(array($email, $pass));
        if ($sth->rowCount() > 0) {
            $_SESSION['logged'] = true;
            return $response->withRedirect(URL . '/admin');
        } else {
            return $this->view->render($response, 'admin/login.phtml', ['error' => 'Dados Inválidos']);
        }
    }

    public function logout($request, $response, array $args) {
        if (isset($_SESSION['logged'])) {
            unset($_SESSION['logged']);
            session_destroy();
        }
        return $response->withRedirect(URL . '/admin');
    }

}