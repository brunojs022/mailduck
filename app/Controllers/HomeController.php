<?php

namespace App\Controllers;

final class HomeController extends DefaultController {

  public function index($request, $response, array $args) {
    return $this->view->render($response, 'home.phtml', ['nome' => 'Bruno Santos']);
  }

}
?>
