<?php

namespace App\Controllers;

final class AdminController extends DefaultController {

    public function index($request, $response, array $args) {
        return $this->view->render($response, 'admin/dashboard.phtml');
    }

}