<?php

namespace App\Controllers;

class DefaultController {

  private $container;

  public function __construct($container) {
        $this->container = $container;
  }

  public function __get($property) {
    return $this->container->{$property} ? $this->container->{$property} : null;
  }

}
