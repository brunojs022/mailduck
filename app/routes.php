<?php

$app->get('/', 'App\Controllers\HomeController:index');
$app->get('/admin/login[/]', 'App\Controllers\LoginController:index');
$app->post('/admin/login', 'App\Controllers\LoginController:login');
$app->get('/admin/logout', 'App\Controllers\LoginController:logout');
$app->get('/admin[/]', 'App\Controllers\AdminController:index')
    ->add(App\Middlewares\AdminMidleware::class);