create table users (
  id Integer not null auto_increment,
  name varchar(255) not null,
  email varchar(255) not null,
  password varchar(255) not null,
  role tinyint not null default 0,
  primary key(id)
);

insert into users values (null, "Admin", "admin@example.com", "81dc9bdb52d04dc20036dbd8313ed055", 0);
